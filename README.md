---
lang: en-us
---

# ARCHIVED

I've archived this project because I want to change some things. In the next project, one device will use one thread. I decided this because devices are I/O bound and the shared state in the device infrastructure could be simplified (or often removed). This will greatly improve the visibility and maintainability of the code.

---

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

This is a long term project. See [v0.1](https://codeberg.org/Fl1tzi/dach-decker/milestone/3854) for things that I require for the first version to be released.

# Microdeck

Microdeck is an open-source software designed for configuring Stream Decks with ease and versatility. It aims to address common pain points found in existing solutions by offering improved rendering speeds, greater customization options, and a focus on stability. Other devices may be added in the future (see [driver](https://codeberg.org/Fl1tzi/deck-driver)) if needed. The only requirement is that the key layout is rectangular and that there are displays on the keys (otherwise other pieces of software would be more useful).
